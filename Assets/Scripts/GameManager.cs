﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Runner {
	public class GameManager : MonoBehaviour {
		public static GameManager Self;
		[SerializeField, Range(1, 100)]
		private int _health = 3;
		[SerializeField]
		private Transform _playerTransform;
		[SerializeField]
		private Transform[] _blocks;
		private int _progress, _currentIndex;
		private float _stepZ = 10f, _lastZ = 90f;
		[SerializeField]
		private float _timer, _upSpeed;
		[SerializeField]
		private Text _progressTxt, _timerTxt, _healthTxt;
		[SerializeField]
		private GameObject _player;
		private PlayerStatsComponent _stats;
		private bool _isDead;

		private void Awake() {
			Self = this;
		}
		private void Start() {
			_progressTxt.text = "0";
			_stats = _player.GetComponent<PlayerStatsComponent>();
		}
		private void Update() {
			if(Input.GetKeyDown(KeyCode.Escape)) {
#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
				Application.Quit();
#endif
			}

			if(!_isDead && _playerTransform.position.y < -1f || _timer <= 0f) {
				PlayerDamage(100);
			}

			if(_health <= 0) {
				_isDead = true;
			}

			_timerTxt.text = _timer.ToString();
			_timer -= Time.deltaTime;

			_healthTxt.text = _health.ToString();

			_stats.MoveSpeed += _upSpeed * Time.deltaTime;
		}
		private void OnDisable() {
			Debugger.Log("Exit Game");
		}
		public void UpdateLevel() {
			_progress++;
			_progressTxt.text = _progress.ToString();

			_lastZ += _stepZ;
			_blocks[_currentIndex].position = new Vector3(0f, 0f, _lastZ);
			_currentIndex++;

			if(_currentIndex >= _blocks.Length) {
				_currentIndex = 0;
			}
		}
		public void PlayerDamage(int count) {
			_health -= count;

			Debugger.Log($"Get Damage ({count})");

			_stats.MoveSpeed -= _stats.MoveSpeed * 30f / 100f;

			if(_health <= 0) {
				Debugger.Log("Game Over");
#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPaused = true;
#elif UNITY_STANDALONE
				Time.timeScale = 0f;
#endif
			}
		}
	}
}