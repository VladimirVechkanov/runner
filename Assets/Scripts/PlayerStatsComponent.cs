﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	public class PlayerStatsComponent : MonoBehaviour {
		[Range(0.1f, 100f)]
		public float MoveSpeed, SideSpeed, JumpForce;
	}
}