﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	public class NewPlayerComponent : BasePlauerComponent {
		private RunnerControls _controls;

		protected override void Start() {
			base.Start();
		}
		void Update() {
			var direction = 0f;
#if UNITY_EDITOR
			direction = _controls.Player.SideMoveArrows.ReadValue<float>() * _stats.SideSpeed * Time.deltaTime;
#elif UNITY_STANDALONE
			direction = _controls.Player.SideMoveWASD.ReadValue<float>() * _stats.SideSpeed * Time.deltaTime;
#endif
			if(direction == 0) return;
			transform.position += transform.right * direction;

			if(direction > 0) {
				Debugger.Log("Get Key \"Right\"");
			}
			else {
				Debugger.Log("Get Key \"Left\"");
			}
		}
		private void Awake() {
			_controls = new RunnerControls();
			_controls.Player.Jump.performed += _ => Jump();
		}
		private void OnEnable() {
			_controls.Player.Enable();
		}
		private void OnDisable() {
			_controls.Player.Disable();
		}
		private void OnDestroy() {
			_controls.Dispose();
		}
	}
}