﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	[RequireComponent(typeof(Rigidbody), typeof(PlayerStatsComponent))]
	public class BasePlauerComponent : MonoBehaviour {
		protected Rigidbody _rigidbody;
		protected PlayerStatsComponent _stats;

		protected virtual void Start() {
			_rigidbody = GetComponent<Rigidbody>();
			_stats = GetComponent<PlayerStatsComponent>();

			StartCoroutine(MoveForward());
		}
		private IEnumerator MoveForward() {
			while(true) {
				transform.position += transform.forward * _stats.MoveSpeed * Time.deltaTime;
				yield return null;
			}
		}
		protected void Jump() {
			_rigidbody.AddForce(transform.up * _stats.JumpForce, ForceMode.Impulse);

			Debugger.Log("Get Key \"Space\"");
		}
	}
}