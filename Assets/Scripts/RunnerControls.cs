// GENERATED AUTOMATICALLY FROM 'Assets/Scripts/RunnerControls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace Runner
{
    public class @RunnerControls : IInputActionCollection, IDisposable
    {
        public InputActionAsset asset { get; }
        public @RunnerControls()
        {
            asset = InputActionAsset.FromJson(@"{
    ""name"": ""RunnerControls"",
    ""maps"": [
        {
            ""name"": ""Player"",
            ""id"": ""111773f4-d3ea-4ef1-bf45-68a182a63d40"",
            ""actions"": [
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""dc8cdf3a-c8ad-4624-b8c6-ae200029809f"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SideMoveWASD"",
                    ""type"": ""Value"",
                    ""id"": ""a7dbee27-a57a-4ee7-bd6f-a491d8a806c3"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""SideMoveArrows"",
                    ""type"": ""Value"",
                    ""id"": ""89eced71-4ebd-48f0-bb5a-094d9a23606b"",
                    ""expectedControlType"": ""Axis"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""02d4845e-b891-4df3-9e2e-774ff53b6728"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""AD"",
                    ""id"": ""d4580f9f-db75-4973-ba44-a561d9fcf858"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveWASD"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""8f7e85ce-4cf7-45f0-bb79-406f6fb6b1e8"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""caab5e32-669b-4eee-8233-8eea6b7a7486"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveWASD"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""Arrows"",
                    ""id"": ""bdb6d832-031a-41ff-b2b5-bf113084f850"",
                    ""path"": ""1DAxis"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveArrows"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""negative"",
                    ""id"": ""6fe57cd2-e400-45cc-9db3-5962e21d3fe2"",
                    ""path"": ""<Keyboard>/leftArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveArrows"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""positive"",
                    ""id"": ""55829440-cc44-426a-b5cf-c559b1c73b8b"",
                    ""path"": ""<Keyboard>/rightArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""SideMoveArrows"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
            // Player
            m_Player = asset.FindActionMap("Player", throwIfNotFound: true);
            m_Player_Jump = m_Player.FindAction("Jump", throwIfNotFound: true);
            m_Player_SideMoveWASD = m_Player.FindAction("SideMoveWASD", throwIfNotFound: true);
            m_Player_SideMoveArrows = m_Player.FindAction("SideMoveArrows", throwIfNotFound: true);
        }

        public void Dispose()
        {
            UnityEngine.Object.Destroy(asset);
        }

        public InputBinding? bindingMask
        {
            get => asset.bindingMask;
            set => asset.bindingMask = value;
        }

        public ReadOnlyArray<InputDevice>? devices
        {
            get => asset.devices;
            set => asset.devices = value;
        }

        public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

        public bool Contains(InputAction action)
        {
            return asset.Contains(action);
        }

        public IEnumerator<InputAction> GetEnumerator()
        {
            return asset.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Enable()
        {
            asset.Enable();
        }

        public void Disable()
        {
            asset.Disable();
        }

        // Player
        private readonly InputActionMap m_Player;
        private IPlayerActions m_PlayerActionsCallbackInterface;
        private readonly InputAction m_Player_Jump;
        private readonly InputAction m_Player_SideMoveWASD;
        private readonly InputAction m_Player_SideMoveArrows;
        public struct PlayerActions
        {
            private @RunnerControls m_Wrapper;
            public PlayerActions(@RunnerControls wrapper) { m_Wrapper = wrapper; }
            public InputAction @Jump => m_Wrapper.m_Player_Jump;
            public InputAction @SideMoveWASD => m_Wrapper.m_Player_SideMoveWASD;
            public InputAction @SideMoveArrows => m_Wrapper.m_Player_SideMoveArrows;
            public InputActionMap Get() { return m_Wrapper.m_Player; }
            public void Enable() { Get().Enable(); }
            public void Disable() { Get().Disable(); }
            public bool enabled => Get().enabled;
            public static implicit operator InputActionMap(PlayerActions set) { return set.Get(); }
            public void SetCallbacks(IPlayerActions instance)
            {
                if (m_Wrapper.m_PlayerActionsCallbackInterface != null)
                {
                    @Jump.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    @Jump.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    @Jump.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnJump;
                    @SideMoveWASD.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveWASD;
                    @SideMoveWASD.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveWASD;
                    @SideMoveWASD.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveWASD;
                    @SideMoveArrows.started -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveArrows;
                    @SideMoveArrows.performed -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveArrows;
                    @SideMoveArrows.canceled -= m_Wrapper.m_PlayerActionsCallbackInterface.OnSideMoveArrows;
                }
                m_Wrapper.m_PlayerActionsCallbackInterface = instance;
                if (instance != null)
                {
                    @Jump.started += instance.OnJump;
                    @Jump.performed += instance.OnJump;
                    @Jump.canceled += instance.OnJump;
                    @SideMoveWASD.started += instance.OnSideMoveWASD;
                    @SideMoveWASD.performed += instance.OnSideMoveWASD;
                    @SideMoveWASD.canceled += instance.OnSideMoveWASD;
                    @SideMoveArrows.started += instance.OnSideMoveArrows;
                    @SideMoveArrows.performed += instance.OnSideMoveArrows;
                    @SideMoveArrows.canceled += instance.OnSideMoveArrows;
                }
            }
        }
        public PlayerActions @Player => new PlayerActions(this);
        public interface IPlayerActions
        {
            void OnJump(InputAction.CallbackContext context);
            void OnSideMoveWASD(InputAction.CallbackContext context);
            void OnSideMoveArrows(InputAction.CallbackContext context);
        }
    }
}
