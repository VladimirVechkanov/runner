﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	public class OldPlayerComponent : BasePlauerComponent {
		protected override void Start() {
			base.Start();
		}
		void Update() {
			if(Input.GetKeyDown(KeyCode.Space)) Jump();

			var direction = Input.GetAxis("Horizontal") * _stats.SideSpeed * Time.deltaTime;
			if(direction == 0) return;
			transform.position += transform.right * direction;

			if(direction > 0) {
				Debugger.Log("Get Key \"Right\"");
			}
			else {
				Debugger.Log("Get Key \"Left\"");
			}
		}
	}
}