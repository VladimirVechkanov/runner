﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Runner {
	public class Buttons : MonoBehaviour {
		private void Start() {
			Debugger.Log("Start Game");
		}
		public void Play() {
			SceneManager.LoadScene("GameScene");

			Debugger.Log("Load GameScene");
		}
		public void Quit() {
#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_STANDALONE
			Application.Quit();
#endif
		}
	}
}