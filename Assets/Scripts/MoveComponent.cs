﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	public class MoveComponent : MonoBehaviour {
		[SerializeField]
		private float _speed;
		private bool _moveLeft = true;

		void Update() {
			if(_moveLeft) {
				transform.position += -transform.right * _speed * Time.deltaTime;
				if(transform.position.x <= -2f) {
					_moveLeft = false;
				}
			}
			else {
				transform.position += transform.right * _speed * Time.deltaTime;
				if(transform.position.x >= 2f) {
					_moveLeft = true;
				}
			}
		}
	}
}