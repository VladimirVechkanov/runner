﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Runner {
	public static class Debugger {
		public static void Log(string message) {
#if UNITY_EDITOR
			Debug.Log(message);
#elif UNITY_STANDALONE
			using(StreamWriter sw = new StreamWriter(Application.dataPath + "/Log.txt", true, System.Text.Encoding.UTF8)) {
				sw.WriteLine("[" + DateTime.Now.ToString("hh:mm:ss") + "] " + message);
			}
#endif
		}
	}
}