﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Runner {
	public class TriggerComponent : MonoBehaviour {
		[SerializeField]
		private bool _isDamage;

		private void OnTriggerEnter(Collider other) {
			if(other.GetComponent<BasePlauerComponent>() == null) return;

			if(_isDamage) {
				GameManager.Self.PlayerDamage(1);
			}
			else {
				GameManager.Self.UpdateLevel();
			}
		}
	}
}